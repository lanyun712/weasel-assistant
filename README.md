# 小狼毫助手超强系列专版

#### 介绍
小狼毫助手超强快码、超强两笔、超强音形系列专用(根据五笔群大佬(https://gitee.com/leeonchiu/rime-tools-for-wubi98)所做更改)

功能简单但很实用，可以提取精准造词，自定义加词，还有一些对小狼毫的简单设置

![](img/1.png)
![](img/2.png)
![](img/3.jpg)
![](img/4.png)